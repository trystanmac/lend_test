<?php

declare(strict_types=1);

namespace LoansTest\Report\Interest;

use DateTime;
use Loans\Model\Tranche as TrancheModel;
use Loans\Report\Interest\Calculate as CalculateInterest;
use PHPUnit\Framework\TestCase;

/**
 * Class CalculateTest
 * @package LoansTest\Report\Interest
 */
class CalculateTest extends TestCase
{
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new CalculateInterest();
    }

    public function testGet(): void
    {
        $trancheMock = $this->createMock(TrancheModel::class);
        $amount = (float) rand(100, 199);

        $now = new DateTime();
        $numberOfDaysInMonth = (int) $now->format('t');

        $investmentDayNumber = rand(1, $numberOfDaysInMonth);
        $trancheInvestmentDateTenDaysInFuture = new DateTime();
        $trancheInvestmentDateTenDaysInFuture->modify('first day of this month');
        $trancheInvestmentDateTenDaysInFuture->modify("+ {$investmentDayNumber} days");

        $beginningMonth = new DateTime();
        $beginningMonth->modify('first day of this month');

        $endMonth = new DateTime();
        $endMonth->modify('last day of this month');

        $interestRate = (float) rand(1, 100) / 100;
        $trancheMock
            ->expects($this->once())
            ->method('interestRate')
            ->willReturn($interestRate);

        $numberOfDaysInvested = $numberOfDaysInMonth - $investmentDayNumber;

        $expectedInterest = ($interestRate * $amount) * ($numberOfDaysInvested / $numberOfDaysInMonth);

        $actualInterest = $this->objectUnderTest->get(
            $trancheMock,
            $amount,
            $trancheInvestmentDateTenDaysInFuture,
            $now
        );

        $this->assertEquals(
            round($expectedInterest, 2),
            $actualInterest
        );
    }

    public function testGetWhenInvestmentDateWasPreviousToThisMonth(): void
    {
        $trancheMock = $this->createMock(TrancheModel::class);
        $amount = (float) rand(100, 199);

        $now = new DateTime();

        $trancheInvestmentDateTenDaysInFuture = new DateTime();
        $trancheInvestmentDateTenDaysInFuture->modify("- 1 month");

        $beginningMonth = new DateTime();
        $beginningMonth->modify('first day of this month');

        $endMonth = new DateTime();
        $endMonth->modify('last day of this month');

        $interestRate = (float) rand(1, 100) / 100;
        $trancheMock
            ->expects($this->once())
            ->method('interestRate')
            ->willReturn($interestRate);

        // Since the investment was made the previous month, the interest will
        // be in place for the complete month
        $expectedInterest = ($interestRate * $amount);

        $actualInterest = $this->objectUnderTest->get(
            $trancheMock,
            $amount,
            $trancheInvestmentDateTenDaysInFuture,
            $now
        );

        $this->assertEquals(
            round($expectedInterest, 2),
            $actualInterest
        );
    }

    public function testGetWithInvestor1Details(): void
    {
        $trancheMock = $this->createMock(TrancheModel::class);
        $amount = 1000;

        $now = new DateTime('10/01/2015');
        $trancheInvestmentDateTenDaysInFuture = new DateTime( '10/03/2015');

        $beginningMonth = new DateTime();
        $beginningMonth->modify('first day of this month');

        $endMonth = new DateTime();
        $endMonth->modify('last day of this month');

        $interestRate = (float) 3 / 100;
        $trancheMock
            ->expects($this->once())
            ->method('interestRate')
            ->willReturn($interestRate);

        $actualInterest = $this->objectUnderTest->get(
            $trancheMock,
            $amount,
            $trancheInvestmentDateTenDaysInFuture,
            $now
        );

        $this->assertEquals(
            28.06,
            $actualInterest
        );
    }

    public function testGetWithInvestor2Details(): void
    {
        $trancheMock = $this->createMock(TrancheModel::class);
        $amount = 500;

        $now = new DateTime('10/01/2015');
        $trancheInvestmentDateTenDaysInFuture = new DateTime( '10/10/2015');

        $beginningMonth = new DateTime();
        $beginningMonth->modify('first day of this month');

        $endMonth = new DateTime();
        $endMonth->modify('last day of this month');

        $interestRate = (float) 6 / 100;
        $trancheMock
            ->expects($this->once())
            ->method('interestRate')
            ->willReturn($interestRate);

        $actualInterest = $this->objectUnderTest->get(
            $trancheMock,
            $amount,
            $trancheInvestmentDateTenDaysInFuture,
            $now
        );

        $this->assertEquals(
            21.29,
            $actualInterest
        );
    }
}