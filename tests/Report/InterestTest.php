<?php

declare(strict_types=1);

namespace LoansTest\Report;

use DateTime;
use Loans\Model\Investor as InvestorModel;
use Loans\Model\Tranche as TrancheModel;
use Loans\Model\Tranche\Ledger as TrancheLedger;
use Loans\Report\Interest as InterestReport;
use Loans\Report\Interest\Calculate as CalculateInterest;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class InterestTest
 * @package LoansTest\Report
 */
class InterestTest  extends TestCase
{
    /** @var InterestReport $objectUnderTest */
    private $objectUnderTest;

    /** @var TrancheLedger | MockObject $trancheLedger */
    private $trancheLedger;

    /** @var CalculateInterest | MockObject $trancheLedger */
    private $interestCalculator;

    /** @var DateTime $dateTimeNow */
    private $dateTimeNow;

    public function setUp(): void
    {
        $this->trancheLedger = $this->createMock(TrancheLedger::class);
        $this->interestCalculator = $this->createMock(CalculateInterest::class);
        $this->dateTimeNow = new DateTime();

        $this->objectUnderTest = new InterestReport($this->trancheLedger, $this->interestCalculator, $this->dateTimeNow);
    }

    public function testGetReport(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);
        $from = new DateTime();
        $to = new DateTime();

        $trancheLedgerMockData = [];

        $trancheMock1 = $this->createMock(TrancheModel::class);
        $amount1 = (float) rand(100, 199);
        $investmentDateTime1 = new DateTime();
        $trancheLedgerMockData[] = [
            'investor' => $investorMock,
            'tranche' => $trancheMock1,
            'amount' => $amount1,
            'dateTime' => $investmentDateTime1,
        ];

        $trancheMock2 = $this->createMock(TrancheModel::class);
        $amount2 = (float) rand(100, 199);
        $investmentDateTime2 = new DateTime();
        $trancheLedgerMockData[] = [
            'investor' => $investorMock,
            'tranche' => $trancheMock2,
            'amount' => $amount2,
            'dateTime' => $investmentDateTime2,
        ];

        $this->trancheLedger
            ->expects($this->once())
            ->method('getInvestorTransactions')
            ->with(
                $this->identicalTo($investorMock),
                $this->identicalTo($from),
                $this->identicalTo($to)
            )
            ->willReturn($trancheLedgerMockData);

        $interest1 = (float) rand(200, 299);
        $this->interestCalculator
            ->expects($this->at(0))
            ->method('get')
            ->with(
                $this->identicalTo($trancheMock1),
                $this->identicalTo($amount1),
                $this->identicalTo($investmentDateTime1),
                $this->identicalTo($this->dateTimeNow)
            )
            ->willReturn($interest1);

        $interest2 = (float) rand(300, 399);
        $this->interestCalculator
            ->expects($this->at(1))
            ->method('get')
            ->with(
                $this->identicalTo($trancheMock2),
                $this->identicalTo($amount2),
                $this->identicalTo($investmentDateTime2),
                $this->identicalTo($this->dateTimeNow)
            )
            ->willReturn($interest2);

        $this->assertEquals(
            ($interest1 + $interest2),
            $this->objectUnderTest->get($investorMock, $from, $to)
        );
    }
}