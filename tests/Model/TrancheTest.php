<?php

declare(strict_types=1);

namespace LoansTest\Model;

use DateTime;
use Exception;
use Loans\Model\Investor as InvestorModel;
use Loans\Model\Loan as LoanModel;
use Loans\Model\Tranche as TrancheModel;
use Loans\Model\Tranche\Ledger as TrancheLedger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class TrancheTest
 *
 *  -   Each tranche has a different monthly interest percentage.
 *  -   Also each tranche has a maximum amount available to invest. So once the maximum is
 *      reached, further investments can't be made in that tranche.
 * 
 * @package LoansTest\Model
 */
class TrancheTest extends TestCase
{
    /** @var TrancheModel $objectUnderTest */
    private $objectUnderTest;

    /** @var TrancheLedger | MockObject $trancheLedgerMock */
    private $trancheLedgerMock;

    /** @var float $maximumInvestment */
    private $maximumInvestment;

    /** @var float $maximumInvestment */
    private $interestRate;

    public function setUp(): void
    {
        $this->trancheLedgerMock = $this->createMock(TrancheLedger::class);
        $this->maximumInvestment = (float) rand(1000, 1999);
        $this->interestRate = (float) rand(10, 20);

        $this->objectUnderTest = new TrancheModel(
            $this->trancheLedgerMock,
            $this->maximumInvestment,
            $this->interestRate
        );
    }

    public function testSetLoanFluentInterface(): void
    {
        $loanMock = $this->createMock(LoanModel::class);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->setLoan($loanMock)
        );
    }

    public function testInvestWithoutLoan(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Error, Tranche has not been attached to a loan.');

        $this->objectUnderTest->invest(
            $this->createMock(InvestorModel::class),
            (float) rand(100, 200),
            new DateTime()
        );
    }

    public function testInvestFluentInterface(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);
        $amount = (float) rand(100, 200);

        $loanMock = $this->createMock(LoanModel::class);
        $this->objectUnderTest->setLoan($loanMock);

        $loanMock
            ->expects($this->once())
            ->method('isOpen')
            ->willReturn(true);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->invest($investorMock, $amount, new DateTime())
        );
    }

    public function testInvest(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);
        $amount = (float) rand(100, 200);
        $investmentDate = new DateTime();

        $investorMock
            ->expects($this->once())
            ->method('debit')
            ->with($amount);

        $this->trancheLedgerMock
            ->expects($this->once())
            ->method('getTrancheTotal')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn((float) 0);

        $this->trancheLedgerMock
            ->expects($this->once())
            ->method('addTransaction')
            ->with(
                $this->identicalTo($investorMock),
                $this->identicalTo($this->objectUnderTest),
                $amount,
                $this->identicalTo($investmentDate)
            );

        $loanMock = $this->createMock(LoanModel::class);
        $this->objectUnderTest->setLoan($loanMock);

        $loanMock
            ->expects($this->once())
            ->method('isOpen')
            ->willReturn(true);

        $this->objectUnderTest->invest($investorMock, $amount, $investmentDate);
    }

    public function testInvestMoreThanMaximum(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);
        $amount = (float) rand(2000, 2999);

        $this->trancheLedgerMock
            ->expects($this->once())
            ->method('getTrancheTotal')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn((float) 0);

        $investorMock
            ->expects($this->never())
            ->method('debit');

        $this->trancheLedgerMock
            ->expects($this->never())
            ->method('addTransaction');

        $loanMock = $this->createMock(LoanModel::class);
        $this->objectUnderTest->setLoan($loanMock);

        $loanMock
            ->expects($this->once())
            ->method('isOpen')
            ->willReturn(true);

        $this->expectException(Exception::class);

        $this->objectUnderTest->invest($investorMock, $amount, new DateTime());
    }

    public function testMultipleInvestmentsUntilMoreThanMaximum(): void
    {
        $loanMock = $this->createMock(LoanModel::class);
        $this->objectUnderTest->setLoan($loanMock);

        $loanMock
            ->expects($this->any())
            ->method('isOpen')
            ->willReturn(true);

        $amountAvailableToInvest = rand(1, 99);
        $this->trancheLedgerMock
            ->expects($this->once())
            ->method('getTrancheTotal')
            ->with(
                $this->identicalTo($this->objectUnderTest)
            )
            ->willReturn($this->maximumInvestment - $amountAvailableToInvest);

        $investorMock = $this->createMock(InvestorModel::class);

        $amount = rand(100, 200);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage(sprintf(
            'Error, maximum investment amount is %s. Maximum investment availability: %s',
            $this->maximumInvestment,
            $amountAvailableToInvest
        ));

        $this->objectUnderTest->invest($investorMock, $amount, new DateTime());
    }

    public function testInvestLoanNotOpen(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);
        $amount = (float) rand(100, 200);

        $this->trancheLedgerMock
            ->expects($this->never())
            ->method('getTrancheTotal');

        $investorMock
            ->expects($this->never())
            ->method('debit')
            ->with($amount);

        $this->trancheLedgerMock
            ->expects($this->never())
            ->method('addTransaction');

        $loanMock = $this->createMock(LoanModel::class);
        $this->objectUnderTest->setLoan($loanMock);

        $loanMock
            ->expects($this->once())
            ->method('isOpen')
            ->willReturn(false);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Error, cannot invest in this Tranche as the related Loan is now closed.');

        $this->objectUnderTest->invest($investorMock, $amount, new DateTime());
    }

    public function testInterestRate(): void
    {
        $this->assertEquals(
            $this->interestRate / 100,
            $this->objectUnderTest->interestRate()
        );
    }
}