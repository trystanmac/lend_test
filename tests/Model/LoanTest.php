<?php

declare(strict_types=1);

namespace LoansTest\Model;

use DateInterval;
use DateTime;
use Loans\Model\Loan as LoanModel;
use Loans\Model\Tranche as TrancheModel;
use PHPUnit\Framework\TestCase;

/**
 * Class LoanTest
 * @package LoansTest\Model
 */
class LoanTest extends TestCase
{
    /** @var LoanModel $objectUnderTest */
    private $objectUnderTest;

    /** @var DateTime $loanStartDate */
    private $loanStartDate;

    /** @var DateTime $loanEndDate */
    private $loanEndDate;

    /** @var DateTime $now */
    private $now;

    public function setUp(): void
    {
        $this->loanStartDate = new DateTime();
        $this->loanEndDate = new DateTime();
        $this->now = new DateTime();

        $this->objectUnderTest = new LoanModel(
            $this->loanStartDate,
            $this->loanEndDate,
            $this->now
        );
    }

    public function testAddTrancheFluentInterface(): void
    {
        $trancheMock = $this->createMock(TrancheModel::class);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->addTranche($trancheMock)
        );
    }

    public function testAddTranche(): void
    {
        $trancheMock = $this->createMock(TrancheModel::class);

        $trancheMock
            ->expects($this->once())
            ->method('setLoan')
            ->with($this->identicalTo($this->objectUnderTest));

        $this->objectUnderTest->addTranche($trancheMock);
    }

    public function testLoanIsOpen(): void
    {
        $this->assertTrue($this->objectUnderTest->isOpen());
    }

    public function testLoanIsOpenStartInThePastAndEndInTheFuture(): void
    {
        $this->loanStartDate->sub(new DateInterval('P1D'));
        $this->loanEndDate->add(new DateInterval('P1D'));

        $this->assertTrue($this->objectUnderTest->isOpen());
    }

    public function testLoanIsNotOpenStartInTheFutureAndEndInThePast(): void
    {
        $this->loanStartDate->add(new DateInterval('P1D'));
        $this->loanEndDate->sub(new DateInterval('P1D'));

        $this->assertFalse($this->objectUnderTest->isOpen());
    }
}