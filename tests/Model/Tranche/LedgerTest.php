<?php

declare(strict_types=1);

namespace LoansTest\Model\Tranche;

use DateInterval;
use DateTime;
use Loans\Model\Investor as InvestorModel;
use Loans\Model\Tranche as TrancheModel;
use Loans\Model\Tranche\Ledger as TrancheLedger;
use PHPUnit\Framework\TestCase;

/**
 * Class LedgerTest
 * @package LoansTest\Model\Tranche
 */
class LedgerTest extends TestCase
{
    /** @var TrancheLedger $objectUnderTest */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new TrancheLedger();
    }

    public function testAddTransactionFluidInterface(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);
        $trancheMock = $this->createMock(TrancheModel::class);
        $amount = (float) rand(100, 199);
        $date = new DateTime();

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->addTransaction($investorMock, $trancheMock, $amount, $date)
        );
    }

    public function testAddTransactionAndGetInvestorTransactions(): void
    {
        $investor1Mock = $this->createMock(InvestorModel::class);
        $investor2Mock = $this->createMock(InvestorModel::class);
        $trancheMock = $this->createMock(TrancheModel::class);
        $amount = (float) rand(100, 199);
        $dateNow = new DateTime();

        $dateTwoDaysAgo = new DateTime();
        $dateTwoDaysAgo->sub(new DateInterval('P2D'));

        $dateFifteenDaysAhead = new DateTime();
        $dateFifteenDaysAhead->add(new DateInterval('P15D'));

        $dateThirtyDaysAhead = new DateTime();
        $dateThirtyDaysAhead->add(new DateInterval('P30D'));

        $dateThirtyOneDaysAhead = new DateTime();
        $dateThirtyOneDaysAhead->add(new DateInterval('P31D'));


        $this->objectUnderTest
            ->addTransaction($investor1Mock, $trancheMock, $amount, $dateNow)
            ->addTransaction($investor1Mock, $trancheMock, $amount, $dateThirtyOneDaysAhead)
            ->addTransaction($investor2Mock, $trancheMock, $amount, $dateNow)
            ->addTransaction($investor2Mock, $trancheMock, $amount, $dateNow)
            ->addTransaction($investor1Mock, $trancheMock, $amount, $dateTwoDaysAgo)
            ->addTransaction($investor1Mock, $trancheMock, $amount, $dateThirtyDaysAhead)
            ->addTransaction($investor1Mock, $trancheMock, $amount, $dateFifteenDaysAhead);

        $expected = [
            [
                'investor' => $investor1Mock,
                'tranche' => $trancheMock,
                'amount' => $amount,
                'dateTime' => $dateNow,
            ],
            [
                'investor' => $investor1Mock,
                'tranche' => $trancheMock,
                'amount' => $amount,
                'dateTime' => $dateThirtyDaysAhead,
            ],
            [
                'investor' => $investor1Mock,
                'tranche' => $trancheMock,
                'amount' => $amount,
                'dateTime' => $dateFifteenDaysAhead,
            ],
        ];

        $this->assertSame(
            $expected,
            $this->objectUnderTest->getInvestorTransactions($investor1Mock, $dateNow, $dateThirtyDaysAhead)
        );
    }

    public function testGetInvestorTransactionsWithoutTransactions(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);

        $expected = [];

        $this->assertSame(
            $expected,
            $this->objectUnderTest->getInvestorTransactions($investorMock, new DateTime(), new DateTime())
        );
    }

    public function testGetTrancheInvestmentTotal(): void
    {
        $investorMock = $this->createMock(InvestorModel::class);
        $tranche1Mock = $this->createMock(TrancheModel::class);
        $tranche2Mock = $this->createMock(TrancheModel::class);
        $amount1 = (float) rand(100, 199);
        $amount2 = (float) rand(100, 199);
        $amount3 = (float) rand(100, 199);
        $date = new DateTime();

        $this->objectUnderTest
            ->addTransaction($investorMock, $tranche1Mock, $amount1, $date)
            ->addTransaction($investorMock, $tranche2Mock, $amount2, $date)
            ->addTransaction($investorMock, $tranche2Mock, $amount3, $date);

        $this->assertEquals(
            ($amount2 + $amount3),
            $this->objectUnderTest->getTrancheTotal($tranche2Mock)
        );
    }

    public function testGetTrancheInvestmentWithoutTransactions(): void
    {
        $trancheMock = $this->createMock(TrancheModel::class);

        $this->assertSame(
            (float) 0,
            $this->objectUnderTest->getTrancheTotal($trancheMock)
        );
    }
}