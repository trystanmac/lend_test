<?php

declare(strict_types=1);

namespace LoansTest\Model;

use Exception;
use Loans\Model\Investor as InvestorModel;
use PHPUnit\Framework\TestCase;

/**
 * Class InvestorTest
 * @package LoansTest\Model
 */
class InvestorTest extends TestCase
{
    /** @var InvestorModel */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new InvestorModel();
    }

    public function testCreditFluentInterface(): void
    {
        $amount = (float) rand(100, 200);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->credit($amount)
        );
    }

    public function testGetBalanceWithoutCredits(): void
    {
        $this->assertSame(
            (float) 0,
            $this->objectUnderTest->balance()
        );
    }

    public function testCreditAndGetBalance(): void
    {
        $amount = (float) rand(100, 200);

        $this->objectUnderTest->credit($amount);

        $this->assertEquals(
            $amount,
            $this->objectUnderTest->balance()
        );
    }

    public function testMultipleCreditsAndGetBalance(): void
    {
        $amount1 = (float) rand(100, 200);
        $amount2 = (float) rand(100, 200);
        $amount3 = (float) rand(100, 200);

        $this->objectUnderTest
            ->credit($amount1)
            ->credit($amount2)
            ->credit($amount3);

        $this->assertEquals(
            ($amount1 + $amount2 + $amount3),
            $this->objectUnderTest->balance()
        );
    }

    public function testDebitFluentInterface(): void
    {
        $amount = (float) rand(100, 200);
        $this->objectUnderTest->credit($amount);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->debit($amount)
        );
    }

    public function testDebitWithZeroBalance(): void
    {
        $amount = (float) rand(100, 200);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Error, insufficient funds');

        $this->objectUnderTest->debit($amount);
    }

    public function testDebitBalance(): void
    {
        $amountToCredit = (float) rand(100, 200);
        $this->objectUnderTest->credit($amountToCredit);

        $amountToDebit = (float) rand(1, 99);

        $this->objectUnderTest->debit($amountToDebit);

        $this->assertEquals(
            ($amountToCredit - $amountToDebit),
            $this->objectUnderTest->balance()
        );
    }
}