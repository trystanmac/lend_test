<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Loans\Model\Investor;
use Loans\Model\Loan;
use Loans\Model\Tranche\Ledger;
use Loans\Model\Tranche;
use Loans\Report\Interest;
use Loans\Report\Interest\Calculate;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /** @var Loan $loan */
    static $loan;

    /** @var Ledger $ledger */
    static $ledger;

    /** @var array  */
    static $tranches = [];

    /** @var array  */
    static $investors = [];

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        // Create the ledge here as no params are needed to create
        // and it is used across the scenario
        self::$ledger = new Ledger();
    }

    /**
     * @Given a loan starts on :startDateString and ends on :endDateString
     */
    public function aLoanStartsOnAndEndsOn($startDateString, $endDateString)
    {
        $startDate = new DateTime($startDateString);
        $ensDate = new DateTime($endDateString);
        // Represents a date in October 2015
        $nowDateTime = new DateTime($startDateString);

        self::$loan = new Loan($startDate, $ensDate, $nowDateTime);
    }

    /**
     * @Given I create tranche :trancheLabel with monthly interest :interest percent and maximum investment :maximumInvestment pounds and attach to the loan
     */
    public function iCreateTrancheWithMonthlyInterestPercentAndMaximumInvestmentPoundsAndAttachToTheLoan($trancheLabel, $interest, $maximumInvestment)
    {
        self::$tranches[$trancheLabel] = new Tranche(self::$ledger, $maximumInvestment, $interest);
        self::$loan->addTranche(self::$tranches[$trancheLabel]);
    }

    /**
     * @Given As Investor “:investorName” with credit of :credit I invest :investAmount pounds on the tranche :trancheName on :investmentDateString
     */
    public function asInvestorIInvestPoundsOnTheTrancheOn($investorName, $credit, $investAmount, $trancheName, $investmentDateString)
    {
        $investor = new Investor();
        $investor->credit($credit);
        $investmentDate = new DateTime($investmentDateString);

        self::$investors[$investorName] = $investor;
        self::$tranches[$trancheName]->invest($investor, $investAmount, $investmentDate);
    }

    /**
     * @Given As Investor “:investorName” with credit of :credit I invest :investAmount pounds on the tranche :trancheName on :investmentDateString and throw a maximum investment error :amountToInvest left
     *
     */
    public function asInvestorWithCreditOfIInvestPoundsOnTheTrancheOnAndThrowAnException($investorName, $credit, $investAmount, $trancheName, $investmentDateString, $amountToInvest)
    {
        $investor = new Investor();
        $investor->credit($credit);
        $investmentDate = new DateTime($investmentDateString);

        self::$investors[$investorName] = $investor;

        $exceptionMsg = '';

        try {
            self::$tranches[$trancheName]->invest($investor, $investAmount, $investmentDate);
        } catch (Exception $e) {
            $exceptionMsg = $e->getMessage();
        }

        Assert::assertEquals(
            sprintf('Error, maximum investment amount is 1000. Maximum investment availability: %s', $amountToInvest),
            $exceptionMsg
        );
    }

    /**
     * @When I run the end of month report on :reportRunDate, Investor “:investorName” has earned interest of :expectedInterestEarned pounds
     */
    public function iRunTheEndOfMonthReportOnInvestorHasEarnedInterestOfPounds($reportRunDate, $expectedInterestEarned, $investorName)
    {
        $interestCalculator = new Calculate();
        $reportRunDate = new DateTime($reportRunDate);
        $interest = new Interest(self::$ledger, $interestCalculator, $reportRunDate);

        $from = clone $reportRunDate;
        $to = clone $reportRunDate;

        $from->modify('first day of this month');
        $to->modify('last day of this month');

        $actualInterestEarned = $interest->get(self::$investors[$investorName], $from, $to);

        Assert::assertEquals($expectedInterestEarned, $actualInterestEarned);
    }
}