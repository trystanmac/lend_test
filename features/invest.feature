Feature: Invest and calculate interest

  Scenario: Calculate the interest generated for two investors using different tranches and check maximum loan thresholds
  Given a loan starts on "01-10-2015" and ends on "15-11-2015"
  And I create tranche "A" with monthly interest "3" percent and maximum investment "1000" pounds and attach to the loan
  And I create tranche "B" with monthly interest "6" percent and maximum investment "1000" pounds and attach to the loan
  And As Investor “1” with credit of "1000" I invest "1000" pounds on the tranche "A" on "03-10-2015"
  And As Investor “2” with credit of "1000" I invest "1" pounds on the tranche "A" on "04-10-2015" and throw a maximum investment error "0" left
  And As Investor “3” with credit of "1000" I invest "500" pounds on the tranche "B" on "10-10-2015"
  And As Investor “4” with credit of "1000" I invest "1100" pounds on the tranche "B" on "25-10-2015" and throw a maximum investment error "500" left
  When I run the end of month report on "31-10-2015", Investor “1” has earned interest of "28.06" pounds
  When I run the end of month report on "31-10-2015", Investor “3” has earned interest of "21.29" pounds

