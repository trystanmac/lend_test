<?php

declare(strict_types=1);

namespace Loans\Model;

use DateTime;
use Exception;
use Loans\Model\Investor as InvestorModel;
use Loans\Model\Loan as LoanModel;
use Loans\Model\Tranche\Ledger as TrancheLedger;

/**
 * Class Tranche
 * @package Loans\Model
 */
class Tranche
{
    /** @var TrancheLedger $trancheLedger */
    private $trancheLedger;

    /** @var LoanModel $loan */
    private $loan;

    /** @var float $maximumInvestment */
    private $maximumInvestment;

    /** @var float $interestRate */
    private $interestRate;

    /**
     * Tranche constructor.
     * @param TrancheLedger $trancheLedger
     * @param float $maximumInvestment
     * @param float $interestRate
     */
    public function __construct(
        TrancheLedger $trancheLedger,
        float $maximumInvestment,
        float $interestRate
    ) {
        $this->trancheLedger = $trancheLedger;
        $this->maximumInvestment = $maximumInvestment;
        $this->interestRate = $interestRate;
    }

    /**
     * @param Loan $loan
     * @return Tranche
     */
    public function setLoan(LoanModel $loan): self
    {
        $this->loan = $loan;
        return $this;
    }

    /**
     * @param Investor $investor
     * @param float $amount
     * @param DateTime $investmentDate
     * @return Tranche
     * @throws Exception
     */
    public function invest(InvestorModel $investor, float $amount, DateTime $investmentDate): self
    {
        if (null === $this->loan) {
            throw new Exception('Error, Tranche has not been attached to a loan.');
        }

        if (true !== $this->loan->isOpen()) {
            throw new Exception(
                'Error, cannot invest in this Tranche as the related Loan is now closed.'
            );
        }

        $totalInLedger = $this->trancheLedger->getTrancheTotal($this);
        if (($totalInLedger + $amount) > $this->maximumInvestment) {
            throw new Exception(sprintf(
                'Error, maximum investment amount is %s. Maximum investment availability: %s',
                $this->maximumInvestment,
                $this->maximumInvestment - $totalInLedger
            ));
        }

        $investor->debit($amount);
        $this->trancheLedger->addTransaction($investor, $this, $amount, $investmentDate);
        return $this;
    }

    /**
     * @return float
     */
    public function interestRate(): float
    {
        return (float) $this->interestRate / 100;
    }
}