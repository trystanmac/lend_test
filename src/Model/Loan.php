<?php

declare(strict_types=1);

namespace Loans\Model;

use Exception;
use DateTime;
use Loans\Model\Tranche as TrancheModel;

/**
 * Class Loan
 * @package Loans\Model
 */
class Loan
{
    /** @var DateTime $startDate */
    private $startDate;

    /** @var DateTime $endDate */
    private $endDate;

    /** @var DateTime $now */
    private $now;

    /**
     * Loan constructor.
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param DateTime $now
     * @throws Exception
     */
    public function __construct(DateTime $startDate, DateTime $endDate, DateTime $now)
    {
        $this->now = $now;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param Tranche $tranche
     * @return $this
     */
    public function addTranche(TrancheModel $tranche): self
    {
        $tranche->setLoan($this);
        return $this;
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        $startInterval = $this->now->diff($this->startDate)->format('%R%a');
        $endInterval = $this->now->diff($this->endDate)->format('%R%a');

        $isOpen = true;

        if ($startInterval{0} == '+' and $endInterval{0} == '-') {
            $isOpen = false;
        }

        return $isOpen;
    }
}