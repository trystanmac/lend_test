<?php

declare(strict_types=1);

namespace Loans\Model\Tranche;

use DateTime;
use Loans\Model\Investor;
use Loans\Model\Tranche;

/**
 * Class Ledger
 * @package Loans\Model\Tranche
 */
class Ledger
{
    private $ledgerData = [];

    /**
     * @param Investor $investor
     * @param Tranche $tranche
     * @param float $amount
     * @param DateTime $dateTime
     * @return Ledger
     */
    public function addTransaction(Investor $investor, Tranche $tranche, float $amount, DateTime $dateTime): self
    {
        $this->ledgerData[] = [
            'investor' => $investor,
            'tranche' => $tranche,
            'amount' => $amount,
            'dateTime' => $dateTime,
        ];

        return $this;
    }

    /**
     * @param Investor $investor
     * @param DateTime $from
     * @param DateTime $to
     * @return array
     */
    public function getInvestorTransactions(Investor $investor, DateTime $from, DateTime $to): array
    {
        $investorTransactions = [];

        foreach ($this->ledgerData as $ledgerDatum) {
            if ($investor !== $ledgerDatum['investor']) {
                continue;
            }

            if ($ledgerDatum['dateTime'] >= $from && $ledgerDatum['dateTime'] <= $to) {
                $investorTransactions[] = $ledgerDatum;
            }
        }

        return $investorTransactions;
    }

    /**
     * @param Tranche $tranche
     * @return float
     */
    public function getTrancheTotal(Tranche $tranche): float
    {
        $total = (float) 0;

        foreach ($this->ledgerData as $ledgerDatum) {
            if ($tranche === $ledgerDatum['tranche']) {
                $total += $ledgerDatum['amount'];
            }
        }

        return $total;
    }
}