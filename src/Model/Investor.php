<?php

declare(strict_types=1);

namespace Loans\Model;

use Exception;

/**
 * Class Investor
 * @package Loans\Model
 */
class Investor
{
    private $ledger = [];

    /**
     * @param float $amount
     * @return Investor
     */
    public function credit(float $amount): self
    {
        $this->ledger[] = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function balance(): float
    {
        return array_sum($this->ledger);
    }

    /**
     * @param float $amount
     * @return Investor
     * @throws Exception
     */
    public function debit(float $amount): self
    {
        if ($this->balance() < $amount) {
            throw new Exception('Error, insufficient funds');
        }

        $this->ledger[] = - $amount;
        return $this;
    }
}