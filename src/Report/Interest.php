<?php

declare(strict_types=1);

namespace Loans\Report;

use DateTime;
use Loans\Model\Investor as InvestorModel;
use Loans\Model\Tranche\Ledger as TrancheLedger;
use Loans\Report\Interest\Calculate as InterestCalculate;

/**
 * Class Interest
 * @package Loans\Report
 */
class Interest
{
    /** @var array $investors */
    private $trancheLedger = [];

    /** @var InterestCalculate $interestCalculator */
    private $interestCalculator;

    /** @var DateTime $dateTimeNow */
    private $dateTimeNow;

    /**
     * Interest constructor.
     * @param TrancheLedger $trancheLedger
     * @param InterestCalculate $interestCalculator
     * @param DateTime $dateTimeNow
     */
    public function __construct(
        TrancheLedger $trancheLedger,
        InterestCalculate $interestCalculator,
        DateTime $dateTimeNow
    ) {
        $this->trancheLedger = $trancheLedger;
        $this->interestCalculator = $interestCalculator;
        $this->dateTimeNow = $dateTimeNow;
    }

    /**
     * @param InvestorModel $investor
     * @param DateTime $from
     * @param DateTime $to
     * @return float
     */
    public function get(InvestorModel $investor, DateTime $from, DateTime $to): float
    {
        $trancheLedgerTransactions = $this->trancheLedger->getInvestorTransactions($investor, $from, $to);

        $interest = 0;

        foreach ($trancheLedgerTransactions as $trancheLedgerTransaction) {
            $interest += $this->interestCalculator->get(
                $trancheLedgerTransaction['tranche'],
                $trancheLedgerTransaction['amount'],
                $trancheLedgerTransaction['dateTime'],
                $this->dateTimeNow
            );
        }

        return $interest;
    }
}