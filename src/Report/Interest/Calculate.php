<?php

declare(strict_types=1);

namespace Loans\Report\Interest;

use DateTime;
use Loans\Model\Tranche as TrancheModel;

/**
 * Class Calculate
 * @package Loans\Report\Interest
 */
class Calculate
{
    /**
     * @param TrancheModel $tranche
     * @param float $amount
     * @param DateTime $investmentDate
     * @param DateTime $from
     * @return float
     */
    public function get(TrancheModel $tranche, float $amount, DateTime $investmentDate, DateTime $from): float
    {
        $from->modify('first day of this month');

        $dayNumberInvested = $from->diff($investmentDate)->format('%d');

        $numberOfDaysInMonth = $from->format('t');

        $numberOfDaysInvested = $numberOfDaysInMonth - $dayNumberInvested;

        /**
         * If the investment was made previous to this month
         * then the investment will be for the whole month.
         *
         * @todo Need to consider if the investment is withdrawn
         * before the end of the month being calculated.
         */
        if ($investmentDate < $from) {
            $numberOfDaysInvested = $numberOfDaysInMonth;
        }

        $interestRate = $tranche->interestRate();

        $interestAmount =  ($interestRate * $amount) * ($numberOfDaysInvested / $numberOfDaysInMonth);

        return round($interestAmount, 2);
    }
}